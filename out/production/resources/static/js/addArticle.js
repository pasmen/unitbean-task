$(function() {
    $('button[type=submit]').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled',true);

        var form = document.forms[0];
        var formData = new FormData(form);

        // Ajax запрос на добавление статьи
        var ajaxReq = $.ajax({
            url : 'add-article',
            type : 'POST',
            data : formData,
            cache : false,
            contentType : false,
            processData : false
        });

        ajaxReq.done(function() {
            window.location.replace("/success");
        });

        ajaxReq.fail(function() {
            alert("Ошибка при добавлении статьи")
        });

        $(this).prop('disabled',false);
    });
});