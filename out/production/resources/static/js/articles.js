var articles = JSON.parse(message);

var options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
};

var listItems = articles.map(function (article) {
    return React.createElement(
        'a',
        { 'class': 'article', href: "/display/" + article._id  },
        React.createElement(
            'div',
            { 'class': 'article-content' },
            React.createElement('img', { src: "./images/" + article.imgName,
                alt: 'content' }),
            React.createElement(
                'h3',
                null,
                article.title
            ),
            React.createElement(
                'h5',
                null,
                new Date(article.date).toLocaleString("ru", options)
            ),
            React.createElement('hr', null),
            React.createElement(
                'h4',
                null,
                article.text.length < 40 ? article.text : article.text.substr(0, 40) + "..."
            )
        )
    );
});