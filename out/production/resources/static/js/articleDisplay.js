var comments = JSON.parse(message);

var options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
};

var listItems = comments.map(function (comment) {
    return React.createElement(
        'div',
        null,
        React.createElement(
            'div',
            { 'class': 'user-pic show-comment-user-pic' },
            React.createElement('img', { src: '../images/User.png' })
        ),
        React.createElement(
            'div',
            { 'class': 'display-articlee-comment' },
            React.createElement(
                'h3',
                null,
                comment.authorName
            ),
            React.createElement(
                'h5',
                null,
                new Date(comment.date).toLocaleString("ru", options)
            ),
            React.createElement(
                'h4',
                null,
                comment.text
            )
        ),
        React.createElement(
            "a",
            { href: "../del-comment/" +  comment._id },
            React.createElement("img", { "class": "del-pic", src: "../images/del.png" })
        )
    );
});