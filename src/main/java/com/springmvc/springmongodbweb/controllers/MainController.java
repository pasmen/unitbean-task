/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springmvc.springmongodbweb.controllers;

import com.google.gson.Gson;
import com.springmvc.springmongodbweb.models.Article;
import com.springmvc.springmongodbweb.models.Comment;
import com.springmvc.springmongodbweb.repositories.ArticleRepository;
import com.springmvc.springmongodbweb.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author didin
 */
@Controller
public class MainController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CommentRepository commentRepository;

    @RequestMapping("/success")
    public String success() {
        return "success";
    }

        @PostMapping("/add-article")
    public ResponseEntity<Object> addArticle(@RequestParam("file") MultipartFile file, @RequestParam String articleTitle, @RequestParam String articleText) throws IOException {
        String imgPath =  System.getProperty("user.dir") + "/src/main/resources/static/images";

        //Если все поля заполненны то сделать запись
            if (!file.getOriginalFilename().isEmpty() && !articleText.isEmpty() && !articleTitle.isEmpty()) {
            Article article = new Article();

            // Сохранить запись в бд
            article.setText(articleText);
            article.setTitle(articleTitle);
            article.setImgName("testpic.jpg");
            article.setDate(new Date());
            article.setImgName(file.getOriginalFilename());
            articleRepository.save(article);

            // Сохранить картинку
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File(imgPath, file.getOriginalFilename())));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
        }else{
            return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Article added",HttpStatus.OK);
    }


    @RequestMapping("/")
    public String index(Model model) {
        Iterable<Article> articles = articleRepository.findAll();
        Gson gson = new Gson();
        String json = gson.toJson(articles);
        model.addAttribute("json", json);
        return "index";
    }

    @RequestMapping("/add")
    public String add() {
        return "add-article";
    }

    @RequestMapping("/del-comment/{commentId}")
    public String delComment(@PathVariable String commentId) {

        Comment comment = commentRepository.findOne(commentId);
        commentRepository.delete(comment);

        return "redirect:/display/" + comment.getArticle();
    }

    @RequestMapping("/del-article/{articleId}")
    public String delArticle(@PathVariable String articleId) {
        Iterable<Comment> comments = commentRepository.findUsersByArticle(articleId);
        commentRepository.delete(comments);
        articleRepository.delete(articleId);

        return "redirect:/";
    }


    @RequestMapping("/create")
    public String create(Model model) {
        return "create";
    }

    @RequestMapping("/display/{articleId}")
    public String display(@PathVariable String articleId, Model model) {
        model.addAttribute("article", articleRepository.findOne(articleId));
        Iterable<Comment> comments = commentRepository.findUsersByArticle(articleId);
        Gson gson = new Gson();
        String json = gson.toJson(comments);
        model.addAttribute("comments", json);
        return "display";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/display/{articleId}")
    public String displayAdd(@PathVariable String articleId, @RequestParam("authorName") String authorName, @RequestParam("commentText") String commentText, Model model) {
        model.addAttribute("article", articleRepository.findOne(articleId));

        Comment comment = new Comment();
        comment.setArticle(articleId);
        comment.setAuthorName(authorName);
        comment.setText(commentText);
        comment.setDate(new Date());
        commentRepository.save(comment);

        return "redirect:/display/" + articleId;
    }
}
