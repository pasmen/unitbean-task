package com.springmvc.springmongodbweb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document(collection = "articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String _id;
    private String title, text, imgName;
    private Date date;

    public Article(String _id, String title, String text, String imgName, Date date) {
        super();
        this._id = _id;
        this.title = title;
        this.text = text;
        this.imgName = imgName;
        this.date = date;
    }

    public Article() {
        super();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
