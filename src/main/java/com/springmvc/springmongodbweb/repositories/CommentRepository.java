/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springmvc.springmongodbweb.repositories;

import com.springmvc.springmongodbweb.models.Comment;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author didin
 */
public interface CommentRepository extends CrudRepository<Comment, String> {
    @Override
    Comment findOne(String id);

    @Query("{article : ?0}")
    Iterable<Comment> findUsersByArticle(String articleId);
}
